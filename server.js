const http = require('http'),
    express = require('express'),
    WebSocket = require('ws'),

    port = 6969,
    server = http.createServer(express),
    wss = new WebSocket.Server({ server })

wss.on('connection', function connection(ws) {
    wss.on('message', function message(data) {
        wss.clients.forEach(function each(client) {
            client !== ws && client.readyState === WebSocket.OPEN
                ? client.send(data)
                : console.log(`Error de Envio`);
        })
    })
})

server.listen(port, function () {
    console.log(`SERVER IS LISTENING ON PORT: ${port}`);
})